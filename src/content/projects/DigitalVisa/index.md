---
date: '2018-01-01'
title: 'DigitalVisa'
image: './DigitalVisa.png'
external: 'http://system.digitalspeeduae.com/'
tech:
  - Django
  - Postgresql
  - Nginx
  - HTML
  - SCSS
show: 'true'
---

Visa and human resources management for more than 30 companies on Dubai with more than 1500 foreign employees.
