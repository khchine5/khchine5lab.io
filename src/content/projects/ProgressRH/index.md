---
date: '2018-03-01'
title: 'Progress RH'
image: './Accueil-Progress-RH-Tunisie.png'
external: 'http://www.progressrh.com/'
tech:
  - Django
  - Sass
  - Postgresql
  - Ubuntu
  - Nginx
  - SEO
show: 'true'
---

An applicant tracking system (ATS) is a software application to publish new jobs and allow candidate to apply. The application manage the whole recruitment process which include:

* Job description and publishing  
* Candidate can create an account or/and apply for a job
* Manage all kind of interviews (Phone,video, face to face)
* Define a declined candidates
* A large database of candidates
