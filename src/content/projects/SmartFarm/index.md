---
date: '2019-01-01'
title: 'Smart Farm'
image: './Smart-Farm-tn.jpg'
external: 'http://smart-farm.sspro.tn/'
tech:
  - Django/Odoo
  - JS
  - HTML
  - Raspberry
  - IOT
show: 'true'
---

This is a new project in the agriculture field. There are 3 IOT projects:

1. [Smart Fresh](http://smart-farm.sspro.tn/smart-fresh)
2. [Smart Sensor](http://smart-farm.sspro.tn/smart-sensor)
3. [Smart Water](http://smart-farm.sspro.tn/smart-water)
4. [Smart Weather Station](http://smart-farm.sspro.tn/smart-weather-station)
