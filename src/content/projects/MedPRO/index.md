---
date: '2018-05-01'
title: 'MedPRO'
image: './MedPRO--Gestion-de-Cabinet-Medical.png'
external: 'http://medpro.com.tn'
tech:
  - Django.
  - Postgresql
  - SAAS
  - HTML
  - Nginx
  - JS
show: 'true'
---

A Saas application to manage doctors activities.
