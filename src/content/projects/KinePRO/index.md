---
date: '2014-04-01'
title: 'KinePRO'
image: './KinePRO-Gestion-des-cabinets-des-kines.png'
external: 'https://kinepro.tn/'
tech:
  - Django
  - HTML
  - CSS
  - Javascript
show: 'true'
---

A Saas application for physiotherapy. It manages all daily activities such appointment planning,tasks , employees... . Now we manage more than 400 customers and more than 2000 patients.
