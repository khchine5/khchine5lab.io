---
title: 'About Me'
avatar: './me.jpg'
skills:
  - Python 2/3
  - Django
  - Flask
  - Postgresql
  - HTML
  - JavaScript
  - HTML & (S)CSS
  - React
  - Vue
  - Node.js
  - Express
  - GraphQL
  - Git
---

Hi! I'm Hamza Khchine, a FullStack (Python/JavaScript) Contract Developer[En/Fr]. a software engineer based in Tunisia, who enjoys building things that live on the internet.

Since 2015, I am working remotely with my clients in France, Belgium, USA and Singapore.