---
date: '2020-02-01'
title: 'FullStack Django/ReactJS developer'
company: 'OMN'
location: 'Paris, France'
range: 'Jan 2020 - Octo 2020'
url: 'https://www.omynote.io/'
---

- OMИ is a cutting-edge artificial-intelligence, Fragrance-dedicated company, that will revolutionize the customer experience in both retail and online.
- AWS services.
- Django-based application.
- Django REST framework.
- PostgreSQL
- Facebook/Google/instagram sign up and login.
- APIs for [Mobile Application](https://play.google.com/store/apps/details?id=com.omn&hl=en&gl=US).
- ReactJS (NextJS) Website/Application
