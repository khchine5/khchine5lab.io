---
date: '2019-09-01'
title: 'Core developer of Lino'
company: 'Rumma & Ko'
location: 'Estonia'
range: 'September 2015, present'
url: 'http://www.saffre-rumma.net'
---

- I am the second core developer of [Lino](http://www.saffre-rumma.net/team/).
- I help us to make the [Lino framework](https://www.lino-framework.org/) better.
- Migrate Lino from [Django](https://www.djangoproject.com/) 1.6 to [Django](https://www.djangoproject.com/) 3.0.2
- Add support of [django channels](https://channels.readthedocs.io/en/latest/) (realtime and async capability) to [Lino framework](https://www.lino-framework.org/)
- Create [Getlino](https://getlino.lino-framework.org/), a new command line tools to install Lino on a fresh Linux.
- Upgrade Lino from Extjs3 to [Extjs6](https://www.sencha.com/products/extjs/).
- Help to move Lino from EXTJS3 to [Reactjs](https://reactjs.org/) UI interface.
- Support belgium customers.
