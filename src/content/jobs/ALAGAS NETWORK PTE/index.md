---
date: '2018-03-01'
title: 'Django developer'
company: 'ALAGAS NETWORK PTE'
location: 'Singapore'
range: 'Jan - March 2017'
url: 'https://alagas.shop/'
---

- Make an internal Django application to manage course applications and management.
