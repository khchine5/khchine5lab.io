---
date: '2014-02-01'
title: 'CTO and lead developer'
company: 'NERbA-TS'
location: 'Tunis, Tunisia'
range: 'Fev 2014 - Dec 2017'
url: 'http://nerba-ts.com/'
---

- Developed and maintained code for in-house and client ERP applications primarily using Python and postgresql.

