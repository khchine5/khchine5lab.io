---
date: '2018-04-01'
title: 'Python Developer'
company: 'Digital Speed Typing'
location: 'United Arab Emirates, Dubai'
range: 'Jan - April 2018'
url: 'http://digitalspeeduae.com/'
---

- Visa and human resources management for more than 30 companies on Dubai.
