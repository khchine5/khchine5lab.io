---
date: '2017-04-01'
title: 'Django developer'
company: 'Web Design By Sami'
location: 'California , USA'
range: 'April - May 2017'
url: 'https://www.webdesignbysami.com/'
---

- Fix and add new features for the website Slamgrams using Django.
