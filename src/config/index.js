module.exports = {
  siteTitle: 'Hamza Khchine | Software Engineer',
  siteDescription:
    'Hamza khchine is a software engineer based in Tunisia who specializes in developing high-quality websites and applications.',
  siteKeywords:
    'Hamza Khchine, khchine5, python,Django, E-Commerce software engineer, front-end engineer, web developer, javascript',
  siteUrl: 'https://khchine5.gitlab.io/',
  siteLanguage: 'en_US',

  googleVerification: 'dGRSwmIZOyyZhvnfDI9YQ0S_9Iz5Qp4H0RmI6h8g2zU',

  name: 'Hamza Khchine',
  location: 'Tunisia',
  email: 'hamzakhchine@gmail.com',
  github: 'https://github.com/khchine5/',
  socialMedia: [
    {
      name: 'Github',
      url: 'https://github.com/khchine5/',
    },
    {
      name: 'Gitlab',
      url: 'https://gitlab.com/khchine5/',
    },

    {
      name: 'Linkedin',
      url: 'https://www.linkedin.com/in/hamza-khchine-10ab6268/',
    },
    {
      name: 'Twitter',
      url: 'https://twitter.com/khchine',
    },
  ],

  navLinks: [
    {
      name: 'About',
      url: '#about',
    },
    {
      name: 'Experience',
      url: '#jobs',
    },
    {
      name: 'Work',
      url: '#projects',
    },
    {
      name: 'Contact',
      url: '#contact',
    },
  ],

  twitterHandle: '@khchine',
  googleAnalyticsID: 'UA-82715901-4',

  navHeight: 100,

  greenColor: '#64ffda',
  navyColor: '#0a192f',
  darkNavyColor: '#020c1b',

  srConfig: (delay = 200) => ({
    origin: 'bottom',
    distance: '20px',
    duration: 500,
    delay,
    rotate: { x: 0, y: 0, z: 0 },
    opacity: 0,
    scale: 1,
    easing: 'cubic-bezier(0.645, 0.045, 0.355, 1)',
    mobile: true,
    reset: false,
    useDelay: 'always',
    viewFactor: 0.25,
    viewOffset: { top: 0, right: 0, bottom: 0, left: 0 },
  }),
};
